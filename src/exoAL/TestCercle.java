package exoAL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TestCercle {
	public static void ajouter(ArrayList<Cercle> l, Cercle...cercles ) {
		for (Cercle c : cercles) {
			l.add(c);
		}
	}
	
	public static void main(String[] args) {
		Cercle c1 = new Cercle(2,9,2.5);
		Cercle c2 = new Cercle(1,9,3.5);
		Cercle c3 = new Cercle (5,3,1.5);
		ArrayList<Cercle> tab = new ArrayList<Cercle>();
		TestCercle.ajouter(tab,c1,c2,c3);
		System.out.println(tab);
		Collections.sort(tab);
		System.out.println("Après le tri sur les rayons :");
		System.out.println(tab);
		Collections.sort(tab, new Comparator <Cercle>()
				{ public int compare (Cercle c1, Cercle c2)
					{
						int resultat;
						double difference = c1.getX() - c2.getX();
						if (difference >0) resultat = 1;
						else if (difference <0) resultat = -1;
						else resultat = 0;
						return resultat;
					}
				}				
			);
		System.out.println("Après le tri sur les abscisses : ");
		System.out.println(tab);
	}
}
