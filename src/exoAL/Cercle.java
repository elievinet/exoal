package exoAL;

public class Cercle implements Comparable<Cercle> {
	private int x,y;
	protected double rayon;
	public Cercle(int x, int y, double rayon) {
		super();
		this.x = x;
		this.y = y;
		this.rayon = rayon;
	}
	public  int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String toString () {
		return "Cercle de centre (" + x + ", " + y +") de rayon " + rayon;
	}
	public int compareTo(Cercle c1) {
		int resultat = 0;
		if (rayon- c1.rayon >0) resultat=1;
		else if (rayon - c1.rayon <0) resultat= -1;
		return resultat;
	}
}
